﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator_project
{
    [DesignTimeVisible(false)]
        public partial class MainPage : ContentPage
        {
        double nbr_1;
        double nbr_2;
        string myoperator;
        int check = 1;


            public MainPage() {
                InitializeComponent();
            }
            void GetNbr(object sender, EventArgs e)
            {
                Button button = (Button)sender;
                string pressed = button.Text;

                if (this.result.Text == "0" || check < 0) {                
                    this.result.Text = "";
                    if (check < 0) // to take ope and note number
                        check = check * -1;
                }
                this.result.Text = this.result.Text + pressed;

                double number;
                if (double.TryParse(this.result.Text, out number)) { // str to nbr
                    this.result.Text = number.ToString("N0");
                    if (check == 1) //to take good nbr
                        nbr_1 = number;
                    else
                        nbr_2 = number;
                }
            }
            void GetOpe(object sender, EventArgs e)
            {
                check = -5; // to take 2nd
                Button button = (Button)sender;
                string pressed = button.Text;
                myoperator = pressed;
            }
            void Egal(object sender, EventArgs e) 
            {
                if (check == 5) {
                double result = 0;
                if (myoperator == "+")
                    result = nbr_1 + nbr_2;
                else if (myoperator == "/")
                    result = nbr_1 / nbr_2;
                else if (myoperator == "x")
                    result = nbr_1 * nbr_2;
                else if (myoperator == "-")
                    result = nbr_1 - nbr_2;
                this.result.Text = result.ToString();
                check = -1;
                nbr_1 = result;
                }
            }
        void Clear(object sender, EventArgs e)
        {
            this.result.Text = "0";
            nbr_1 = 0;
            nbr_2 = 0;
            check = 1;
        }
    }   
}
